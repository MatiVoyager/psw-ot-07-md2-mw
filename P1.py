import pgzrun

WIDTH = 900
HEIGHT = 600

def draw():

    #screen.draw.line((10,10),(790,590),'yellow')
    #screen.draw.rect(Rect((10,10),(780,580)),'red')
    #screen.draw.circle((400,300),290,'pink')

    z = 31

    for i in 0, 120, 420, 520, 620:
        screen.draw.circle((200+i, 395), 20, '#22941E')
        screen.draw.circle((190+i, 435), 20, '#22941E')
        screen.draw.circle((170+i, 380), 30, '#22941E')
        screen.draw.circle((140+i, 420), 40, '#22941E')

        screen.draw.line((170+i, 470), (165+i, 540), 'brown')

        screen.draw.line((170+i, 470), (190+i, 435), 'brown')
        screen.draw.line((190+i, 435), (200+i, 395), 'brown')
        screen.draw.line((190+i, 435), (170+i, 380), 'brown')
        screen.draw.line((170+i, 470), (140+i, 420), 'brown')

    for i in 0, 100:
        screen.draw.line((350+i, 340), (350+i, 540), 'gray')

    screen.draw.line((350, 340), (370, 340), 'gray')
    screen.draw.line((390, 340), (410, 340), 'gray')
    screen.draw.line((430, 340), (450, 340), 'gray')

    screen.draw.line((370, 340), (370, 370), 'gray')
    screen.draw.line((390, 340), (390, 370), 'gray')
    screen.draw.line((410, 340), (410, 370), 'gray')
    screen.draw.line((430, 340), (430, 370), 'gray')

    screen.draw.line((370, 370), (390, 370), 'gray')
    screen.draw.line((410, 370), (430, 370), 'gray')

    screen.draw.rect(Rect((370, 390), (25, 25)), 'gray')
    screen.draw.rect(Rect((410, 435), (25, 25)), 'gray')
    screen.draw.rect(Rect((370, 480), (25, 61)), 'gray')

    for i in range(1, 29):
        screen.draw.filled_rect(Rect((z, 541), (28, 28)), '#89421A')
        z = z + 30

    for i in range(1, 29):
        screen.draw.rect(Rect((30 * i, 540), (30, 30)), '#0B7F3A')
        screen.draw.line((30 * i, 541), (27+(30*i), 568), '#2E2723')

pgzrun.go()

#MateuszWaligorski
