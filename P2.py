import pgzrun

BRICK = 32
WIDTH = 25 * BRICK #800
HEIGHT = 18 * BRICK  #587

def draw():
    screen.clear()
    screen.blit('sky', (0, 0))

    for i in range(25):
        screen.blit('dirt', (BRICK * (i) + BRICK * 5, BRICK * 17))
        screen.blit('dirt', (BRICK * (i) + BRICK * 4, BRICK * 16))
        screen.blit('grass', (BRICK * (i) + BRICK * 3, BRICK * 15))

    for i in range(5):
        screen.blit('stone', (BRICK * (i), BRICK * 17))

    for i in range(4):
        screen.blit('stone', (BRICK * (i), BRICK * 16))

    for i in range(3):
        screen.blit('stone', (BRICK * (i), BRICK * 15))

    for i in range(2):
        screen.blit('stone', (BRICK * (i), BRICK * 14))

    screen.blit('fog', (0, BRICK * 15))

    screen.blit('wood', (BRICK * 6, BRICK * 14))
    screen.blit('wood', (BRICK * 6, BRICK * 13))
    screen.blit('leaves', (BRICK * 6, BRICK * 12))

    screen.blit('leaves', (BRICK * 5, BRICK * 12))
    screen.blit('leaves', (BRICK * 7, BRICK * 12))
    screen.blit('leaves', (BRICK * 4, BRICK * 11))
    screen.blit('leaves', (BRICK * 5, BRICK * 11))
    screen.blit('leaves', (BRICK * 6, BRICK * 11))
    screen.blit('leaves', (BRICK * 7, BRICK * 11))
    screen.blit('leaves', (BRICK * 8, BRICK * 11))
    screen.blit('leaves', (BRICK * 4, BRICK * 10))
    screen.blit('leaves', (BRICK * 5, BRICK * 10))
    screen.blit('leaves', (BRICK * 6, BRICK * 10))
    screen.blit('leaves', (BRICK * 7, BRICK * 10))
    screen.blit('leaves', (BRICK * 8, BRICK * 10))
    screen.blit('leaves', (BRICK * 5, BRICK * 9))
    screen.blit('leaves', (BRICK * 6, BRICK * 9))
    screen.blit('leaves', (BRICK * 7, BRICK * 9))

    screen.blit('wood', (BRICK * 6 + BRICK * 14, BRICK * 14))
    screen.blit('wood', (BRICK * 6 + BRICK * 14, BRICK * 13))
    screen.blit('leaves', (BRICK * 6 + BRICK * 14, BRICK * 12))

    screen.blit('leaves', (BRICK * 5 + BRICK * 14, BRICK * 12))
    screen.blit('leaves', (BRICK * 7 + BRICK * 14, BRICK * 12))
    screen.blit('leaves', (BRICK * 4 + BRICK * 14, BRICK * 11))
    screen.blit('leaves', (BRICK * 5 + BRICK * 14, BRICK * 11))
    screen.blit('leaves', (BRICK * 6 + BRICK * 14, BRICK * 11))
    screen.blit('leaves', (BRICK * 7 + BRICK * 14, BRICK * 11))
    screen.blit('leaves', (BRICK * 8 + BRICK * 14, BRICK * 11))
    screen.blit('leaves', (BRICK * 4 + BRICK * 14, BRICK * 10))
    screen.blit('leaves', (BRICK * 5 + BRICK * 14, BRICK * 10))
    screen.blit('leaves', (BRICK * 6 + BRICK * 14, BRICK * 10))
    screen.blit('leaves', (BRICK * 7 + BRICK * 14, BRICK * 10))
    screen.blit('leaves', (BRICK * 8 + BRICK * 14, BRICK * 10))
    screen.blit('leaves', (BRICK * 5 + BRICK * 14, BRICK * 9))
    screen.blit('leaves', (BRICK * 6 + BRICK * 14, BRICK * 9))
    screen.blit('leaves', (BRICK * 7 + BRICK * 14, BRICK * 9))

    screen.blit('stonebrick', (BRICK * 10, BRICK * 14))
    screen.blit('stonebrick', (BRICK * 10, BRICK * 13))
    screen.blit('stonebrick', (BRICK * 10, BRICK * 12))
    screen.blit('stonebrickmoss', (BRICK * 10, BRICK * 11))
    screen.blit('stonebrickmoss', (BRICK * 10, BRICK * 10))
    screen.blit('stonebrick', (BRICK * 10, BRICK * 9))
    screen.blit('stonebrick', (BRICK * 10, BRICK * 8))

    screen.blit('irondoor1', (BRICK * 11, BRICK * 13))
    screen.blit('irondoor2', (BRICK * 11, BRICK * 14))
    screen.blit('stonebrick', (BRICK * 11, BRICK * 12))
    screen.blit('stonebrickmoss', (BRICK * 11, BRICK * 11))
    screen.blit('stonebrick', (BRICK * 11, BRICK * 10))
    screen.blit('stonebrickmoss', (BRICK * 11, BRICK * 9))

    screen.blit('stonebrick', (BRICK * 12, BRICK * 14))
    screen.blit('stonebrick', (BRICK * 12, BRICK * 13))
    screen.blit('stonebrickmoss', (BRICK * 12, BRICK * 12))
    screen.blit('stonebrickmoss', (BRICK * 12, BRICK * 11))
    screen.blit('stonebrickmoss', (BRICK * 12, BRICK * 10))
    screen.blit('stonebrick', (BRICK * 12, BRICK * 9))
    screen.blit('stonebrick', (BRICK * 12, BRICK * 8))

    screen.blit('stonebrick', (BRICK * 13, BRICK * 14))
    screen.blit('stonebrickmoss', (BRICK * 13, BRICK * 13))
    screen.blit('stonebrick', (BRICK * 13, BRICK * 12))
    screen.blit('stonebrickmoss', (BRICK * 13, BRICK * 11))
    screen.blit('stonebrick', (BRICK * 13, BRICK * 10))
    screen.blit('stonebrick', (BRICK * 13, BRICK * 9))

    screen.blit('stonebrick', (BRICK * 14, BRICK * 14))
    screen.blit('stonebrick', (BRICK * 14, BRICK * 13))
    screen.blit('stonebrickmoss', (BRICK * 14, BRICK * 12))
    screen.blit('stonebrickmoss', (BRICK * 14, BRICK * 11))
    screen.blit('stonebrickmoss', (BRICK * 14, BRICK * 10))
    screen.blit('stonebrickmoss', (BRICK * 14, BRICK * 9))
    screen.blit('stonebrickmoss', (BRICK * 14, BRICK * 8))



pgzrun.go()

#MateuszWaligorski