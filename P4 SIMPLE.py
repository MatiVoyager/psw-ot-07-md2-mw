import pgzrun

WIDTH = 600
HEIGHT = 600

alien = Actor('alien', (50, 50))

def draw():
    screen.clear()
    alien.draw()

def update():
    if keyboard.a:
        alien.x -= 1
    elif keyboard.w:
        alien.y -= 1
    elif keyboard.s:
        alien.y += 1
    elif keyboard.d:
        alien.x += 1

pgzrun.go()