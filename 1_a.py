import operator

def D(znak):
    d = dict()
    for l in znak:
        if l.isalpha() == True:
            if l not in d:
                d[l] = 1
            else:
                d[l] = d[l] + 1
    return max(d, key=d.get)

def B(znak):
    d = dict()
    for l in znak:
        if l.isalpha() == True:
            if l not in d:
                d[l] = 1
            else:
                d[l] = d[l] + 1
    return d

def A(znak):
    d = dict()
    for l in znak:
        if l not in d:
            d[l] = 1
        else:
            d[l] = d[l] + 1
    return d

def main():
    W = input(str('Podaj lancuch: '))
    print('podpunkt a: ', (A(W)))
    print('podpunkt b: ', (B(W)))
    print('podpunkt c: ', (B(W.casefold())))
    print('podpunkt d: ', (D(W)))

main()

# Mateusz Waligorski - 03.07.2020