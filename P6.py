import pgzrun

WIDTH = 600
HEIGHT = 600
beep1 = tone.create('E4', 0.5)
beep2 = tone.create('A#5', 0.5)
beep3 = tone.create('E1', 0.5)
beep4 = tone.create('A#1', 0.5)

def draw():
    screen.draw.rect(Rect((10, 10), (25, 25)), 'gray')
    screen.draw.rect(Rect((45, 10), (25, 25)), 'gray')
    screen.draw.rect(Rect((80, 10), (25, 25)), 'gray')
    screen.draw.rect(Rect((115, 10), (25, 25)), 'gray')

    screen.draw.text("1", (20, 15), color="gray")
    screen.draw.text("2", (55, 15), color="gray")
    screen.draw.text("3", (90, 15), color="gray")
    screen.draw.text("4", (125, 15), color="gray")

def update():
    if keyboard.k_1:
        beep1.play()
    elif keyboard.k_2:
        beep2.play()
    elif keyboard.k_3:
        beep3.play()
    elif keyboard.k_4:
        beep4.play()

pgzrun.go()