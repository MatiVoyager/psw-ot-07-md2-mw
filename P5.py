import pgzrun

WIDTH = 600
HEIGHT = 600

alien = Actor('alien', (50, 50))

def draw():
    screen.clear()
    screen.blit('sky3', (0, 0))
    alien.draw()

    screen.draw.rect(Rect((20, 20), (60, 60)), 'gray')
    screen.draw.rect(Rect((340, 270), (60, 60)), 'gray')
    screen.draw.rect(Rect((150, 200), (60, 60)), 'gray')
    screen.draw.rect(Rect((120, 500), (60, 60)), 'gray')
    screen.draw.rect(Rect((420, 440), (60, 60)), 'gray')



    screen.draw.text("START", (20, 80), color="gray")
    screen.draw.text("MARK 2", (340, 330), color="gray")
    screen.draw.text("MARK 1", (150, 260), color="gray")
    screen.draw.text("MARK 3", (120, 560), color="gray")
    screen.draw.text("MARK 4", (420, 500), color="gray")

def update():
    if keyboard.s:
        animate(alien, tween='accel_decel', duration=2, pos=(50, 50))
    elif keyboard.k_2:
        animate(alien, tween='accel_decel', duration=2, pos=(370, 300))
    elif keyboard.k_1:
        animate(alien, tween='accel_decel', duration=2, pos=(180, 230))
    elif keyboard.k_3:
        animate(alien, tween='accel_decel', duration=2, pos=(150, 530))
    elif keyboard.k_4:
        animate(alien, tween='accel_decel', duration=2, pos=(450, 470))

pgzrun.go()