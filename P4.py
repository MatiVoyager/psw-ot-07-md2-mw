import pgzrun

WIDTH = 600
HEIGHT = 600

alien = Actor('alien', (50, 50))

counter = 1
w_pressed = False
s_pressed = False
a_pressed = False
d_pressed = False

def on_key_down(key):
    global w_pressed
    if key == keys.W:
        print("PRESSED W")
        w_pressed = True
    global a_pressed
    if key == keys.A:
        print("PRESSED A")
        a_pressed = True
    global s_pressed
    if key == keys.S:
        print("PRESSED S")
        s_pressed = True
    global d_pressed
    if key == keys.D:
        print("PRESSED D")
        d_pressed = True

def on_key_up(key):
    global w_pressed
    if key == keys.W:
        print("RELEASED W")
        w_pressed = False
    global a_pressed
    if key == keys.A:
        print("RELEASED A")
        a_pressed = False
    global s_pressed
    if key == keys.S:
        print("RELEASED S")
        s_pressed = False
    global d_pressed
    if key == keys.D:
        print("RELEASED D")
        d_pressed = False

def update():
    global counter
    if w_pressed:
        counter = counter + 1
        alien.y -= 2
        if alien.top > HEIGHT:
            alien.bottom = 0
        elif alien.bottom < 0:
            alien.top = HEIGHT
    if a_pressed:
        counter = counter + 1
        alien.x -= 2
        if alien.left > WIDTH:
            alien.right = 0
        elif alien.right < 0:
            alien.right = WIDTH
    if s_pressed:
        counter = counter + 1
        alien.y += 2
        if alien.top > HEIGHT:
            alien.bottom = 0
        elif alien.bottom < 0:
            alien.top = HEIGHT
    if d_pressed:
        counter = counter + 1
        alien.x += 2
        if alien.left > WIDTH:
            alien.right = 0
        elif alien.right < 0:
            alien.right = WIDTH


def draw():
    screen.clear()
    screen.blit('sky3', (0, 0))
    alien.draw()
    screen.draw.text("LICZNIK RUCHÓW " + str(counter), (10, 10))

pgzrun.go()