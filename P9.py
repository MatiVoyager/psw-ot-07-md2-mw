import pgzrun
import time

WIDTH = 600
HEIGHT = 600

alien = Actor('alien', (50, 50))

def draw():
    screen.clear()
    screen.blit('sky3', (0, 0))
    alien.draw()

def update():
    if keyboard.a:
        alien.x -= 2
    elif keyboard.w:
        alien.y -= 2
    elif keyboard.s:
        alien.y += 2
    elif keyboard.d:
        alien.x += 2

def on_mouse_down(pos):
    if alien.collidepoint(pos):
        set_alien_hurt()

def set_alien_hurt():
    alien.image = 'alien_hurt'
    #sounds.eep.play()
    clock.schedule_unique(set_alien_normal, 1.0)

def set_alien_normal():
    alien.image = 'alien'

pgzrun.go()