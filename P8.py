import pgzrun

WIDTH = 600
HEIGHT = 600

counter = 1
m_pressed = False


def on_key_down(key):
    global m_pressed
    if key == keys.M:
        print("PRESSED M")
        m_pressed = True


def on_key_up(key):
    global m_pressed
    if key == keys.M:
        print("RELEASED M")
        m_pressed = False


def draw():
    screen.clear()
    screen.blit('sky4', (0, 0))

    screen.draw.rect(Rect((20, 20), (75, 25)), 'gray')
    screen.draw.text("press m", (27, 23), color="gray")

    if m_pressed:
        screen.draw.filled_rect(Rect((20, 20), (75, 25)), 'red')
        screen.draw.rect(Rect((20, 20), (75, 25)), 'black')
        screen.draw.text("press m", (27, 23), color="black")
        music.play_once('track.wav')


pgzrun.go()