import pgzrun
import time

WIDTH = 600
HEIGHT = 600
beep1 = tone.create('A3', 0.25)
beep2 = tone.create('A3', 0.25)
beep3 = tone.create('A#3', 0.25)
beep4 = tone.create('A#3', 0.25)
beep5 = tone.create('A4', 0.20)
beep6 = tone.create('A#4', 0.20)
beep7 = tone.create('A3', 0.25)
beep8 = tone.create('A3', 0.25)

def draw():
    screen.draw.rect(Rect((10, 10), (25, 25)), 'gray')
    screen.draw.rect(Rect((45, 10), (25, 25)), 'gray')
    screen.draw.rect(Rect((80, 10), (25, 25)), 'gray')
    screen.draw.rect(Rect((115, 10), (25, 25)), 'gray')

    screen.draw.text("1", (20, 15), color="gray")
    screen.draw.text("2", (55, 15), color="gray")
    screen.draw.text("3", (90, 15), color="gray")
    screen.draw.text("4", (125, 15), color="gray")

def update():
    beep1.play()
    time.sleep(0.5)
    beep2.play()
    time.sleep(0.5)
    beep3.play()
    time.sleep(0.5)
    beep4.play()
    time.sleep(0.5)
    beep5.play()
    time.sleep(0.5)
    beep6.play()
    time.sleep(0.5)


pgzrun.go()